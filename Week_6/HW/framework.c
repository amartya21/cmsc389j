#define _GNU_SOURCE

/* Most imports for things you might need, Do Not Delete any */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<errno.h>
#include<sys/fcntl.h>
#include<dlfcn.h>
#include<dirent.h>

/* Compile: gcc -Wall -fPIC -shared -o evil.so framework.c -ldl */
/* -ldl must be last!!! */

static int (*o_open)(const char *, int);
static void init (void) __attribute__ ((constructor));

/* loaded and executed when ld runs */
void init(void){
	/* Get handle on original open */
   	o_open=dlsym(RTLD_NEXT, "open");
}

/* Try: LD_PRELOAD=./evil.so cat framework.c */
int open(const char *pathname, int flags, ...){ 
	/* example */
	printf("Hello world\n");

 	/* Your code here */ 
  
	/* Preserve functionality */
	return o_open(pathname, flags);
}
