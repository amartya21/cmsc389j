#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* gcc -O0 -fno-stack-protector lab2.c */

int authenticate(char *buf) {
   int canary = 0x12345678;
   int authenticated = 0;
   char password[256];
   int i;
   
   /* Copy over the password */
   strcpy(password, buf);

   for (i = 0; i < 8; i++) {
      password[i] ^= 0x20;
   }
   
   /* Compare against the password */
   if(strcmp(password, "\x50\x41\x53\x53\x11\x12\x13\x10") == 0) {
      authenticated = 1;
   }
  
   if (canary != 0x12345678){
	return 0;
   }
   return authenticated;	
}

int main (){
   char buf[1000] = {0};

   printf("\n\nPassword: ");
   scanf("%1000s", buf);
   if (authenticate(buf)){
      printf("\n\nAuthenticated!\n");
   } else {
      printf("\n\nFailed Password...\n");
   }

   return 0;
}
